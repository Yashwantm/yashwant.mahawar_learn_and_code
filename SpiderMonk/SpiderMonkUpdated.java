package com.itt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class SpiderMonk 
{
	int totalSpiderAtZeroSelectSpiderAtOne[]=new int[2];// at index 1 total no of spider and at index 2 how many spider have to select
	int totalSpiderSelectSpiderCount=0;
	Queue<Spider> spiderQueue = new LinkedList<Spider>();
	public SpiderMonk() throws NumberFormatException, IOException // initialize all values
	{
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		for(String userinput : bufferedReader.readLine().split(" "))
			totalSpiderAtZeroSelectSpiderAtOne[totalSpiderSelectSpiderCount++] = Integer.parseInt(userinput);
		int spiderPowerIndex=1;
		for(String power : bufferedReader.readLine().split(" "))
		{	
			Spider singleSpider = new Spider();
			singleSpider.setIndex(spiderPowerIndex++);
			singleSpider.setPower(Integer.parseInt(power));
			spiderQueue.add(singleSpider);
		} 
	}

	public static void main(String[] args) throws NumberFormatException, IOException 
	{
		SpiderMonk spiderMonk = new SpiderMonk();
		for(int selctSpiderForTime=0;selctSpiderForTime<spiderMonk.totalSpiderAtZeroSelectSpiderAtOne[1];selctSpiderForTime++)
		{
			Queue<Spider> selectedSpiders = spiderMonk.selectFirstSomeSpiders(spiderMonk.spiderQueue);
			Spider maxPowerSpider = spiderMonk.findMaxPowerSpiderFromSelectedSpiders(selectedSpiders);
			spiderMonk.removeMaxPower(maxPowerSpider,selectedSpiders);
			spiderMonk.decreaseRestSpiderPowersByOne(selectedSpiders);
			spiderMonk.enqueSpidersAfterMaxRemoval(selectedSpiders);
		}
	}


	private void enqueSpidersAfterMaxRemoval(Queue<Spider> selectedpowers) 
	{
		spiderQueue.addAll(selectedpowers);
	}
	private void decreaseRestSpiderPowersByOne(Queue<Spider> selectedSpiders)
	{
		for(Spider selectedSpider: selectedSpiders)
		{
			if(selectedSpider.getPower()-1>=0)
			selectedSpider.setPower(selectedSpider.getPower()-1);
		}
	}
	private Queue<Spider> removeMaxPower(Spider MaxPowerSpider,Queue<Spider> selectedSpiders) 
	{
		System.out.print(MaxPowerSpider.getIndex()+" ");
		selectedSpiders.remove(MaxPowerSpider);
		return selectedSpiders;
				
	}

	private Spider findMaxPowerSpiderFromSelectedSpiders(Queue<Spider> selectedpowers) 
	{
		int MAX = -1000;
		Spider maxSpider = null;
		Iterator<Spider> iterator = selectedpowers.iterator();
		while(iterator.hasNext())
		{
			Spider currentSpider = iterator.next();
			if(currentSpider.getPower()>MAX)
			{
				maxSpider = currentSpider;
				MAX = currentSpider.getPower();
			}
		}
		return maxSpider;
	}

	private Queue<Spider> selectFirstSomeSpiders(Queue<Spider> spiderQueue)
	{
		Queue<Spider> firstSomeSelectedSpiders = new LinkedList<Spider>();
		int availableSpidersToSelect = totalSpiderAtZeroSelectSpiderAtOne[1];
		if(spiderQueue.size() <= totalSpiderAtZeroSelectSpiderAtOne[1])
		{
			availableSpidersToSelect = spiderQueue.size();
		}
		for(int i=0;i<availableSpidersToSelect;i++)
		{
			firstSomeSelectedSpiders.add(spiderQueue.poll());
		}
		return firstSomeSelectedSpiders;
	}
}
class Spider
{
	int power;
	int index;
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
}