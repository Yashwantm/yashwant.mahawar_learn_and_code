package testcase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.tests.AllTestsTest.JUnit4Test;
public class BinarySearchTreeTest extends JUnit4Test
{
	@Test
	public void testTreeHeightWhenNull()
	{
		BinarySearchTree BinarySearchTree = new BinarySearchTree();
		bstNode rootNode = null;
		assertEquals(0, BinarySearchTree.treeHeight(rootNode));
	}
	@Test
	public void testTreeHeightWithLeftChlid()
	{
		BinarySearchTree BinarySearchTree = new BinarySearchTree();
		bstNode rootNode = null;
		rootNode = BinarySearchTree.insertnode(rootNode, 5);
		rootNode = BinarySearchTree.insertnode(rootNode, 4);
		rootNode = BinarySearchTree.insertnode(rootNode, 3);
		rootNode = BinarySearchTree.insertnode(rootNode, 2);
		rootNode = BinarySearchTree.insertnode(rootNode, 1);
		assertEquals(5, BinarySearchTree.treeHeight(rootNode));
	}
	@Test
	public void testTreeHeightWithRightChild()
	{
		BinarySearchTree BinarySearchTree = new BinarySearchTree();
		bstNode rootNode = null;
		rootNode = BinarySearchTree.insertnode(rootNode, 1);
		rootNode = BinarySearchTree.insertnode(rootNode, 2);
		rootNode = BinarySearchTree.insertnode(rootNode, 3);
		rootNode = BinarySearchTree.insertnode(rootNode, 4);
		rootNode = BinarySearchTree.insertnode(rootNode, 5);
		assertEquals(5, BinarySearchTree.treeHeight(rootNode));
	}
	@Test
	public void testTreeHeightWithLeftRightChild()
	{
		BinarySearchTree BinarySearchTree = new BinarySearchTree();
		bstNode rootNode = null;
		rootNode = BinarySearchTree.insertnode(rootNode, 8);
		rootNode = BinarySearchTree.insertnode(rootNode, 3);
		rootNode = BinarySearchTree.insertnode(rootNode, 6);
		rootNode = BinarySearchTree.insertnode(rootNode, 5);
		rootNode = BinarySearchTree.insertnode(rootNode, 7);
		rootNode = BinarySearchTree.insertnode(rootNode, 4);
		assertEquals(5, BinarySearchTree.treeHeight(rootNode));
	}
	@Test
	public void testTreeHeightWithRightLeftChild()
	{
		BinarySearchTree BinarySearchTree = new BinarySearchTree();
		bstNode rootNode = null;
		rootNode = BinarySearchTree.insertnode(rootNode, 2);
		rootNode = BinarySearchTree.insertnode(rootNode, 8);
		rootNode = BinarySearchTree.insertnode(rootNode, 3);
		rootNode = BinarySearchTree.insertnode(rootNode, 12);
		rootNode = BinarySearchTree.insertnode(rootNode, 7);
		rootNode = BinarySearchTree.insertnode(rootNode, 6);
		rootNode = BinarySearchTree.insertnode(rootNode, 5);
		rootNode = BinarySearchTree.insertnode(rootNode, 4);
		assertEquals(7, BinarySearchTree.treeHeight(rootNode));
	}
	@Test
	public void testNewNodeCheckNotNull()
	{
		BinarySearchTree BinarySearchTree = new BinarySearchTree();
		assertNotNull(BinarySearchTree.newNode(5));
	}
	@Test
	public void testNewNodeCheckNodeValue()
	{
		BinarySearchTree BinarySearchTree = new BinarySearchTree();
		assertEquals(5,(BinarySearchTree.newNode(5)).Value);
		assertNull((BinarySearchTree.newNode(5)).leftChild);
		assertNull((BinarySearchTree.newNode(5)).rightChild);
	}
	@Test
	public void testInsertNodeWhenTreeEmpty()
	{
		bstNode bstNode = null;
		BinarySearchTree BinarySearchTree = new BinarySearchTree();
		assertEquals(1, BinarySearchTree.treeHeight(BinarySearchTree.insertnode(bstNode, 5)));
	}
}
