package bst;

import java.util.Scanner;

public class binarySearchTree 
{
	public bstNode insertnode(bstNode node,int Value)
	{
		if(node==null)
			return newNode(Value);
		else if(Value<node.Value)
			node.leftChild = insertnode(node.leftChild, Value);
		else if(Value>node.Value)
			node.rightChild = insertnode(node.rightChild, Value);
		return node;
	}

	private bstNode newNode(int nodeValue) 
	{
		bstNode node = new bstNode();
		node.Value = nodeValue;
		node.leftChild=null;
		node.rightChild=null;
		return node;
	}
	@SuppressWarnings("unused")
	private void inOrderTraversal(bstNode node)
	{
		if(node != null)
		{
			inOrderTraversal(node.leftChild);
			System.out.print(node.Value+" ");
			inOrderTraversal(node.rightChild);
		}
	}
	
	@SuppressWarnings("unused")
	private void postOrderTraversal(bstNode node)
	{
		if(node != null)
		{
			postOrderTraversal(node.leftChild);
			postOrderTraversal(node.rightChild);
			System.out.print(node.Value+" ");
		}
	}
	@SuppressWarnings("unused")
	private void preOrderTraversal(bstNode node)
	{
		if(node != null)
		{
			System.out.print(node.Value+" ");
			preOrderTraversal(node.leftChild);
			preOrderTraversal(node.rightChild);
		}
	}
	private int treeHeight(bstNode node)
	{
		int leftTreeHeight,rightTreeHeight;
		if(node==null)
			return 0;
		leftTreeHeight = treeHeight(node.leftChild);
	    rightTreeHeight = treeHeight(node.rightChild);
	    if(leftTreeHeight > rightTreeHeight)
	        return leftTreeHeight+1;
	    else
	        return rightTreeHeight+1;
	}
	
	public static void main(String[] args) 
	{
		binarySearchTree binarySearchTree = new binarySearchTree();
		bstNode rootNode = null;
		Scanner sc = new Scanner(System.in);
		int numberOfElements = sc.nextInt();
		for(int index=0;index<numberOfElements;index++)
		{
			if(rootNode==null)
			{
				rootNode = binarySearchTree.insertnode(rootNode, sc.nextInt());
			}
			else
			{
				binarySearchTree.insertnode(rootNode, sc.nextInt());
			}
		}
		System.out.print(binarySearchTree.treeHeight(rootNode));
		sc.close();
	}
}
