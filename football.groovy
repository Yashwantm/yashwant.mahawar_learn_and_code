class GroovyHelloWorld
{
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
    static int[] player=[0,0]
    static int[] HandOffAndStartPlayer = new int[2]
    static void main(String[] args)
    {
        int Test_Case = br.readLine() as Integer
        1.upto(Test_Case)
                {
                    firstcase(); // to find how many passes and who has the ball at the starting
                    1.upto(HandOffAndStartPlayer[0])
                            {

                                def input = br.readLine();
                                if(input.split(" ")[0] == "p" || input.split(" ")[0] == "P")
                                {
                                    player[1]=player[0]
                                    player[0]=input.split(" ")[1] as Integer

                                }
                                else if(input.split(" ")[0] == "b" || input.split(" ")[0] == "B")
                                {
                                    int a=player[0]
                                    player[0]=player[1]
                                    player[1]=a

                                }
                            }
                    println "Player ${player[0]}"
                }
    }
    static def firstcase()
    {
        int i=0
        br.readLine().toString().split(" ").each {HandOffAndStartPlayer[i++]=Integer.parseInt(it) }
        player[0] = HandOffAndStartPlayer[1]
    }
}