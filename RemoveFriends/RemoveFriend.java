package com.itt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class RemoveFriend
{
	int testCase,noOfFriends,friendsDecidedToDelete;
	LinkedList<Integer> christieFriendList = new LinkedList<Integer>();
	BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
	
	public RemoveFriend() throws Exception //constructor to take input for testCase
	{
		testCase = Integer.parseInt(bufferReader.readLine());
	}
	private void DisplayFriend() 
	{
		while(!christieFriendList.isEmpty())
		{
			System.out.print(christieFriendList.removeFirst()+" ");
		}
		System.out.println();
	}
	private void RemoveFriendProcess(String[] friendsPopularityInput) 
	{
		for(int i=0;i<noOfFriends;i++)
		{
			int currentFriendPopularity = Integer.parseInt(friendsPopularityInput[i]);
			while(!christieFriendList.isEmpty() && friendsDecidedToDelete!=0 && christieFriendList.getLast() < currentFriendPopularity)
			{
				christieFriendList.removeLast();
				friendsDecidedToDelete--;
			}
			christieFriendList.addLast(currentFriendPopularity);
		}
	}
	private void getNoOfFriends_RemovingFriendsNo() throws IOException 
	{
		String input = bufferReader.readLine();
		noOfFriends = Integer.parseInt(input.split(" ")[0]);
		friendsDecidedToDelete= Integer.parseInt(input.split(" ")[1]);
	}
	private String[] getFriendPopularity() throws IOException 
	{
		return bufferReader.readLine().split(" ");
	}
	void StartRemovingFriends() throws IOException 
	{
		while(testCase-->0)
		{
			getNoOfFriends_RemovingFriendsNo();
			RemoveFriendProcess(getFriendPopularity());
			DisplayFriend();		
		}
	}
	public static void main(String[] args) throws Exception
	{
		RemoveFriend RemoveFriend = new RemoveFriend(); //constructor called to take testCase as a input
		RemoveFriend.StartRemovingFriends();
	}
}