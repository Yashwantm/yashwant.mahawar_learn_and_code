import groovy.json.JsonSlurperClassic
import org.apache.commons.io.FilenameUtils

@NonCPS
def getAllEnvironments(rootPath) {
    def list = []
    for (subPath in rootPath.list()) {
        list << FilenameUtils.removeExtension(subPath.getName())
    }
    return list
}

def getProjectName(def environment, def jobName) {
    def requestConfig = readFile(".jenkins/config/$environment"+".json")
    def projectList = new JsonSlurperClassic().parseText(requestConfig)['pipeline']['jobs'].keySet()
    for(String projectName: projectList) {
        if(jobName.indexOf(projectName) >= 0){
            return projectName
        }
    }
}

def getCurrentEnvironment(def jobName, def rootPath) {
    def environmentList = getAllEnvironments(rootPath)
    for (String environment: environmentList) {
        if(jobName.indexOf(environment) >= 0){
            return environment
        }
    }
}

def getNodeName(def environment, def jobName) {
    def projectName = getProjectName(environment, jobName)
    def requestConfig = readFile(".jenkins/config/$environment"+".json")
    def projectDetails = new JsonSlurperClassic().parseText(requestConfig)
    return projectDetails['pipeline']['jobs'][projectName]['label']
}

def createFilePath(def path) {
    if (env['NODE_NAME'].equals("master") || env['NODE_NAME'].equals(null)) {
        File localPath = new File(path)
        return new hudson.FilePath(localPath);
    } else {
        return new hudson.FilePath(Jenkins.getInstance().getComputer(env['NODE_NAME']).getChannel(), path);
    }
}

def getEnvironmentConfigFile(jobName, environments) {
    for (String environment: environments) {
        if(jobName.indexOf("/"+environment+"/") >= 0){
            def props = readFile(".jenkins/config/$environment"+".json")
            return new JsonSlurperClassic().parseText(props)
        }
    }
}

def loadEnvironment(def environment) {
    def props = readFile(".jenkins/config/$environment"+".json")
    return new JsonSlurperClassic().parseText(props)
}

def runScript(fileName,parameters=""){
    if (fileName.indexOf(".bat") >= 0) {
        bat(".jenkins/scripts/$fileName "+parameters)
    } else if(fileName.indexOf(".sh") >=0 ){
        sh(".jenkins/scripts/$fileName "+parameters)
    }else if(fileName.indexOf(".ps1") >=0 ){
        powershell(".jenkins/scripts/$fileName "+parameters)
    }

}

