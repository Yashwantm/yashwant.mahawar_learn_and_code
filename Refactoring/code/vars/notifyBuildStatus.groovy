def call(String buildStatus, String stackName,String mailrecipients) {

    def subject = "${buildStatus}: ${stackName} : Pipeline Job '${env.JOB_NAME}'"
    def details = """<p> Build <b>${buildStatus} </b>: on <b>${stackName}</b> Environment <br> Pipeline Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':
            <br><br> Check console output at <br>${env.BUILD_URL}<br><br>

    Best Regards,<br>
    Jenkins</p>
    """

    emailext (
            to: mailrecipients,
            subject: subject,
            body: details,
            mimeType: 'text/html',
            attachLog: true
    )
}