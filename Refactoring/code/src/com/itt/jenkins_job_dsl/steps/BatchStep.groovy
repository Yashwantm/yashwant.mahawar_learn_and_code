package com.itt.jenkins_job_dsl.steps

class BatchStep extends BuildStep {
    String scriptfilepath = ""

    @Override
    String getType() {
        return "BatchStep"
    }

    @Override
    void create(context) {
        context.with {
                batchFile(scriptfilepath)
        }
    }
}