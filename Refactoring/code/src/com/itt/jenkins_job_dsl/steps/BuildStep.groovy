package com.itt.jenkins_job_dsl.steps

abstract class BuildStep {
    String name
    String targetFile
    String buildConfiguration
    String targetFolder
    String type

    abstract void create(context)

    abstract String getType()
}
