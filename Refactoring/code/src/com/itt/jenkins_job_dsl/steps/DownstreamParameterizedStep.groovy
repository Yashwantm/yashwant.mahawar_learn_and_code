package com.itt.jenkins_job_dsl.steps

class DownstreamParameterizedStep extends BuildStep {
    String job

    @Override
    String getType() {
        return "DownstreamParameterizedStep"
    }

    @Override
    void create(context) {
        context.with {
            downstreamParameterized {
                trigger(job) {
                    parameters {
                        currentBuild()
                    }
                }
            }
        }
    }

}