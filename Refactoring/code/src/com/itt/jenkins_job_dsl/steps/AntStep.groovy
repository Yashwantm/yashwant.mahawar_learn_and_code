package com.itt.jenkins_job_dsl.steps

class AntStep extends BuildStep {
    String antScriptfilepath = ""
    String antTargetName = ""
    @Override
    String getType() {
        return "AntStep"
    }

    @Override
    void create(context) {
        context.with {
            ant {
                target(antTargetName)
                antInstallation('Ant')
                buildFile(antScriptfilepath)
            }
        }
    }
}