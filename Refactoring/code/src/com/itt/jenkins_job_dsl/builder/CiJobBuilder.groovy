package com.itt.jenkins_job_dsl.builder

import com.itt.jenkins_job_dsl.ExtendedChoiceParam
import com.itt.jenkins_job_dsl.Param
import com.itt.jenkins_job_dsl.StringParam
import com.itt.jenkins_job_dsl.ChoiceParam
import com.itt.jenkins_job_dsl.PasswordParam
import com.itt.jenkins_job_dsl.config.GitConfig
import com.itt.jenkins_job_dsl.steps.BuildStep
import com.itt.jenkins_job_dsl.publishers.Publisher
import com.itt.jenkins_job_dsl.helper.ExtendedChoiceParamHelper
import com.itt.jenkins_job_dsl.helper.PasswordParamHelper
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job

class CiJobBuilder {

    String name
    String displayName
    String description
    List<GitConfig> scms
    String label = "master"
    boolean checkoutRetry = false
    List<Param> parameters
    List<Closure> parameterClosures
    List<BuildStep> buildSteps
    String downstreamName
    Boolean bitbucketPush
    String scmPollTrigger
    boolean createJobAsDisabled = false
    boolean cleanBeforeBuild = false
    boolean archiveArtifacts = false
    String archiveArtifactsPattern = "**/*.*"
    List<Publisher> publishers
    boolean blockOnDownStreamProjects = false
    boolean blockOnUpStreamProjects = false
    int buildsToKeep
    def password_param = new PasswordParamHelper()
    def extended_choice_param = new ExtendedChoiceParamHelper()


    Job build(DslFactory dslFactory) {

        dslFactory.job(name) {

            label(label)

            wrappers {
                timestamps()
            }

            if (checkoutRetry) {
                checkoutRetryCount(5) /* a simple retry for git issues */
            }

            if(buildsToKeep)
                logRotator {
                    numToKeep(buildsToKeep)
                }

            description "Build job for ${name}, do not edit."

            if(description) {
                description(description)
            }
            if (cleanBeforeBuild) {
                wrappers {
                    preBuildCleanup()
                }
            }
            if (parameters) {
                parameters {
                    parameters.each { param ->
                        if(param instanceof StringParam) {
                            stringParam(param.name, param.defaultValue, param.description)
                        } else if (param instanceof ChoiceParam) {
                            choiceParam(param.name, param.valueList, param.description)
                        } else if(param instanceof PasswordParam) {
                            configure password_param.password_parameter(param.name, param.defaultValue, param.description)
                        } else if (param instanceof ExtendedChoiceParam) {
                            configure extended_choice_param.extended_choice_parameter(param.name, param.description, param.quoteValue, param.saveJSONParameterToFile, param.visibleItemCount, param.type, param.value, param.multiSelectDelimiter)
                        }
                    }
                }
            }
            else if(parameterClosures){
                parameterClosures.each {
                    parameters it
                }
            }

            if(blockOnDownStreamProjects) {
                blockOnDownstreamProjects()

            }

            if(blockOnUpStreamProjects) {
                blockOnUpstreamProjects()
            }

            if (createJobAsDisabled) {
                disabled()
            }
            if (scms) {
                scm {
                    git {
                        scms.each { g ->
                            remote {
                                url(g.gitRepository)
                                if(g.gitCredentialId)
                                    credentials(g.gitCredentialId)
                                if(g.gitRefspec) {
                                    refspec(g.gitRefspec)
                                }
                            }
                            if(g.gitBranch) {
                                branch(g.gitBranch)
                            }
                        }
                    }
                }
            }

            triggers {
                if(bitbucketPush) {
                    bitbucketPush()
                }
                if(scmPollTrigger) {
                    scm scmPollTrigger

                }
            }

            if (buildSteps) {
                buildSteps.each { step ->
                    steps {
                        step.create delegate
                    }
                }
            }
            publishers {
                if(publishers) {
                    publishers.each { p ->
                        p.create delegate
                    }
                }
                if (archiveArtifacts) {
                    archiveArtifacts {
                        pattern(archiveArtifactsPattern)
                        onlyIfSuccessful(true)
                    }
                }
                if (downstreamName) {
                    downstream(downstreamName, 'SUCCESS')
                }
            }
        }
    }
}
