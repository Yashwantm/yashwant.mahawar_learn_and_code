package com.itt.jenkins_job_dsl.builder

import com.itt.jenkins_job_dsl.ExtendedChoiceParam
import com.itt.jenkins_job_dsl.BitbucketPullRequest
import com.itt.jenkins_job_dsl.StringParam
import com.itt.jenkins_job_dsl.Param
import com.itt.jenkins_job_dsl.ChoiceParam
import com.itt.jenkins_job_dsl.PasswordParam
import com.itt.jenkins_job_dsl.config.GitConfig
import com.itt.jenkins_job_dsl.helper.ExtendedChoiceParamHelper
import com.itt.jenkins_job_dsl.helper.PasswordParamHelper
import com.itt.jenkins_job_dsl.helper.BitbucketPullRequestHelper
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job

public class CiPipelineBuilder implements Serializable {
    String name
    List<Param> parameters
    String displayName
    String description
    String label = "master"
    String pipeline
    String cronTrigger
    String scmPollTrigger
    Boolean bitbucketPush
    GitConfig gitConfig
    String jenkinsFilePath
    Integer buildsToKeep
    Map<String, Object> cloneOptions
    List<String> sparseCheckoutPath
    Boolean gitLfsPull
    def password_param = new PasswordParamHelper()
    def extended_choice_param = new ExtendedChoiceParamHelper()
    def bitbucketPullRequestHelper = new BitbucketPullRequestHelper()
    BitbucketPullRequest bitbucketPullRequestTrigger

    Job build(DslFactory dslFactory) {
        dslFactory.pipelineJob(name) {

            if(buildsToKeep)
            logRotator {
                numToKeep(buildsToKeep)
            }

            if (parameters) {
                parameters {
                    parameters.each { param ->
                        if(param instanceof StringParam) {
                            stringParam(param.name, param.defaultValue, param.description)
                        } else if (param instanceof ChoiceParam) {
                            choiceParam(param.name, param.valueList, param.description)
                        } else if(param instanceof PasswordParam) {
                            configure password_param.password_parameter(param.name, param.defaultValue, param.description)
                        } else if (param instanceof ExtendedChoiceParam) {
                            configure extended_choice_param.extended_choice_parameter(param.name, param.description, param.quoteValue, param.saveJSONParameterToFile, param.visibleItemCount, param.type, param.value, param.multiSelectDelimiter)
                        }
                    }
                }
            }
            if(bitbucketPullRequestTrigger)
            {
                configure bitbucketPullRequestHelper.bitbucketPullRequest(bitbucketPullRequestTrigger.credID,bitbucketPullRequestTrigger.repoOwner,bitbucketPullRequestTrigger.repoName,bitbucketPullRequestTrigger.declineIfBuildFails,bitbucketPullRequestTrigger.cronScheduling)
            }

            label(label)

            triggers {
                if (bitbucketPush) {
                    bitbucketPush()
                }
                if (cronTrigger) {
                    cron cronTrigger
                }
                if (scmPollTrigger) {
                    scm scmPollTrigger
                }
            }

            if (displayName) {
                displayName(displayName)
            }

            if (description) {
                description(description)
            }
            definition {
                if (gitConfig && jenkinsFilePath) {
                    cpsScm {
                        scm {
                            git {
                                branch(gitConfig.gitBranch)
                                remote {
                                    url gitConfig.gitRepository
                                    credentials(gitConfig.gitCredentialId)
                                    refspec(gitConfig.gitRefspec)
                                }
                                extensions {
                                    if (cloneOptions) {
                                        cloneOptions {
                                            honorRefspec(cloneOptions.honorRefspec)
                                            shallow(cloneOptions.shallow)
                                            reference(cloneOptions.reference)
                                            timeout(cloneOptions.timeout)
                                        }
                                    }
                                }
                                if (sparseCheckoutPath) {
                                    configure { git ->
                                        git / 'extensions' / 'hudson.plugins.git.extensions.impl.SparseCheckoutPaths' {
                                            sparseCheckoutPaths {
                                                sparseCheckoutPath.each { checkoutPath ->
                                                    'hudson.plugins.git.extensions.impl.SparseCheckoutPath' {
                                                        path("${checkoutPath}")
                                                    }
                                                }
                                            }
                                        }
                                        if (gitLfsPull) {
                                            git / 'extensions' / 'hudson.plugins.git.extensions.impl.GitLFSPull'
                                        }
                                    }
                                }
                            }
                        }
                        scriptPath jenkinsFilePath
                    }
                } else {
                    cps { script pipeline }
                }
            }
        }
    }
}
