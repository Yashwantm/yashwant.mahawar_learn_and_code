package com.itt.jenkins_job_dsl

abstract class Param {
    String name
    String description
}
