package com.itt.jenkins_job_dsl

class BitbucketPullRequest extends Param{
    def cronScheduling = ""
    String credID=""
    String userNames = ""
    String passwords = ""
    String repoOwner=""
    String repoName=""
    String ciKeys='jenkinsPR'
    String ciNames='Jenkins'
    String branchFilter = ""
    boolean rebuildIfDestinationBracnchChange = false
    boolean approveIfBuildSuccess = false
    boolean declineIfBuildFails = false
    boolean cancleOutDatedJobs = false
}