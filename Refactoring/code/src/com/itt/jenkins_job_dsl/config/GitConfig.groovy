package com.itt.jenkins_job_dsl.config

public class GitConfig implements Serializable{
    String gitRepository
    String gitBranch
    String gitCredentialId
    String gitRefspec
    Boolean triggerOnBitbucketPush = false
}
