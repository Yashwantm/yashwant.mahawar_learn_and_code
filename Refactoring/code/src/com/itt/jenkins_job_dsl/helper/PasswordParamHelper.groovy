package com.itt.jenkins_job_dsl.helper
import hudson.util.Secret

class PasswordParamHelper{
    static Closure password_parameter(paramName, paramDefaultValue, paramDescription ) {
        def password = Secret.fromString(paramDefaultValue)
        def secret = password.getEncryptedValue()
        return {
            it / 'properties'/ 'hudson.model.ParametersDefinitionProperty' / 'parameterDefinitions' << 'hudson.model.PasswordParameterDefinition'{
                name(paramName)
                description(paramDescription)
                'defaultValue'(secret)
            }
        }
    }
}