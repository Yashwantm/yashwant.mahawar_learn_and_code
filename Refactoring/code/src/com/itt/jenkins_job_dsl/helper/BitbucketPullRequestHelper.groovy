package com.itt.jenkins_job_dsl.helper

class BitbucketPullRequestHelper{

    static Closure bitbucketPullRequest(
            String credID='',
            String repoOwner='',
            String repoName='',
            boolean declineIfBuildFails=false,
            String cronScheduling="",
            String userNames="",
            String passwords="",
            String ciNames="",
            String ciKeys="",
            String branchFilter="",
            boolean rebuildIfDestinationBracnchChange=false,
            boolean approveIfBuildSuccess="",
            boolean cancleOutDatedJobs=false
    ) {
        return {
            it / 'properties'/ 'org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty'/ triggers/ 'bitbucketpullrequestbuilder.bitbucketpullrequestbuilder.BitbucketBuildTrigger'( plugin: 'bitbucket-pullrequest-builder@1.4.26'){
                spec('')
                cron(cronScheduling)
                credentialsId(credID)
                username('')
                password('')
                repositoryOwner(repoOwner)
                repositoryName(repoName)
                branchesFilter('')
                branchesFilterBySCMIncludes('')
                ciKey('')
                ciName(ciNames)
                ciSkipPhrases('')
                checkDestinationCommit('')
                approveIfSuccess('')
                cancelOutdatedJobs('')
                declineIfFailure(declineIfBuildFails)
            }
        }
    }
}