package com.itt.jenkins_job_dsl.helper

class ExtendedChoiceParamHelper{
    static Closure extended_choice_parameter(String paramName, String paramDescription, boolean paramQuoteValue, boolean paramSaveJSONParameterToFile, int paramVisibleItemCount, String paramType, String paramValue, String paramDelimiter ) {
        return {
            it / 'properties'/ 'hudson.model.ParametersDefinitionProperty' / 'parameterDefinitions' << 'com.cwctravel.hudson.plugins.extended__choice__parameter.ExtendedChoiceParameterDefinition'(plugin: 'extended-choice-parameter@0.76'){
                name(paramName)
                description(paramDescription)
                quoteValue(paramQuoteValue)
                saveJSONParameterToFile(paramSaveJSONParameterToFile)
                visibleItemCount(paramVisibleItemCount)
                type(paramType)
                value(paramValue)
                multiSelectDelimiter(paramDelimiter)
            }
        }
    }
}