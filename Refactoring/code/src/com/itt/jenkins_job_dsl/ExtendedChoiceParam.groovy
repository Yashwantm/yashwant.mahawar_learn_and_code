package com.itt.jenkins_job_dsl

class ExtendedChoiceParam extends Param{
    String type
    String value
    String projectName
    String propertyFile
    String groovyScript
    String groovyScriptFile
    String bindings
    String groovyClasspath
    String propertyKey
    String defaultValue
    String defaultPropertyFile
    String defaultGroovyScript
    String defaultGroovyScriptFile
    String defaultBindings
    String defaultGroovyClasspath
    String defaultPropertyKey
    String descriptionPropertyValue
    String descriptionPropertyFile
    String descriptionGroovyScript
    String descriptionGroovyScriptFile
    String descriptionBindings
    String descriptionGroovyClasspath
    String descriptionPropertyKey
    String javascriptFile
    String javascript
    boolean saveJSONParameterToFile
    boolean quoteValue
    int visibleItemCount
    String multiSelectDelimiter
}