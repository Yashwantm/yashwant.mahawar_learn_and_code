package com.itt.jenkins_job_dsl.publishers

class DownstreamParameterized extends Publisher {
    String job
    String condition = 'SUCCESS'

    @Override
    void create(context) {
        context.with {
            downstreamParameterized {
                trigger(job) {
                    condition condition
                }
            }
        }
    }
}