package com.itt.jenkins_job_dsl.publishers

import com.itt.jenkins_job_dsl.publishers.ConditionalAction

class FlexiblePublish extends Publisher {

    List<ConditionalAction> conditionalActions

    @Override
    void create(context) {
        context.with {
            def conditions = conditionalActions.collect()
            flexiblePublish {
                conditions.each { conditionalAction ->
                    conditionalAction.create delegate
                }
            }
        }
    }
}