package com.itt.jenkins_job_dsl.publishers

abstract class Publisher {

    abstract void create(context)
}
