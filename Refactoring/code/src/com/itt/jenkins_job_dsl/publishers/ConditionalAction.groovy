package com.itt.jenkins_job_dsl.publishers

import com.itt.jenkins_job_dsl.publishers.DownstreamParameterized

class ConditionalAction extends Publisher {

    Boolean alwaysRun
    Boolean and
    Boolean batch
    String batch_command
    Boolean booleanCondition
    String token
    Boolean expression
    String regEx
    String expLabel
    Boolean neverRun
    Boolean not
    Boolean or
    Boolean shell
    String shell_command
    Boolean downstreamParameterized
    DownstreamParameterized downstream

    @Override
    void create(context) {
        context.with {
            conditionalAction {
                condition {
                    // Runs the build steps no matter what.
                    if (alwaysRun) {
                        alwaysRun()
                    }

                    // Runs the build steps if all of the contained conditions would run.
                    if (and) {
                        and {}
                    }

                    // Runs a Windows batch script for checking the condition.
                    if (batch) {
                        batch(batch_command)
                    }

                    // Expands the Token Macro and run the build step if it evaluates to true.
                    if (booleanCondition) {
                        booleanCondition(token)
                    }

                    // Runs the build steps if the expression matches the label.
                    if (expression) {
                        expression(regEx, expLabel)
                    }

                    // Does not run the build steps.
                    if (neverRun) {
                        neverRun()
                    }

                    // Inverts the result of the selected condition.
                    if (not) {
                        not {}
                    }

                    // Runs the build steps if any of the contained conditions would run.
                    if (or) {
                        or {}
                    }

                    // Runs a shell script for checking the condition.
                    if (shell) {
                        shell(shell_command)
                    }
                }
                publishers {
                    if (downstreamParameterized) {
                        downstream.create delegate
                    }
                }
                runner("DontRun")
            }
        }
    }
}