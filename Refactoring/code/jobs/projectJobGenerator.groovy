import com.itt.jenkins_job_dsl.ExtendedChoiceParam
import com.itt.jenkins_job_dsl.BitbucketPullRequest
import com.itt.jenkins_job_dsl.builder.CiPipelineBuilder
import com.itt.jenkins_job_dsl.config.GitConfig
import com.itt.jenkins_job_dsl.StringParam
import com.itt.jenkins_job_dsl.ChoiceParam
import com.itt.jenkins_job_dsl.PasswordParam
import groovy.io.FileType
import groovy.json.JsonSlurperClassic
import org.apache.commons.io.FilenameUtils

def allProjectDetails = new JsonSlurperClassic().parseText(readFileFromWorkspace('resources/com/itt/jenkins_job_dsl/repoConfig.json'))
def projects = []
if("${ProjectToBeBuilt}".equals("All")) {
    projects = allProjectDetails['projects'].keySet()
} else {
    projects.add(ProjectToBeBuilt)
}
def parent_folder = allProjectDetails['parentFolderName']

folder("${parent_folder}")
projects.each { project_name ->
    def project = allProjectDetails['projects'][project_name]
    def envs = getEnvironments("${project_name}/.jenkins/config")
    folder("$parent_folder/${project_name}")

    envs.each {
        env = it
        folder("$parent_folder/$project_name/$env")

        def envDetails = getConfigJson("$project_name/.jenkins/config/$env" + ".json")
        def jobNames = []
        jobNames = envDetails.pipeline.jobs.keySet()
        project.put('name', "$project_name")

        def parameterList = []
        def bitbucketPullRequestValues

        jobNames.each {
            parameterList.clear()
            def jobDetails = envDetails.pipeline.jobs."$it"
            if(checkIfKeyExists(jobDetails, "parameters")){
                if(jobDetails.parameters.choice) {
                    jobDetails.parameters.choice.each {
                        parameterList.add(new ChoiceParam(name: it.key, valueList: Arrays.asList(it.value.split("\\s*,\\s*"))))
                    }
                }
                if(jobDetails.parameters.string) {
                    jobDetails.parameters.string.each {
                        parameterList.add(new StringParam(name: it.key, defaultValue: it.value))
                    }
                }
                if(jobDetails.parameters.password) {
                    jobDetails.parameters.password.each {
                        parameterList.add(new PasswordParam(name: it.key, defaultValue: it.value))
                    }
                }
                if(jobDetails.parameters.extendedChoice) {
                    jobDetails.parameters.extendedChoice.each {
                        parameterList.add(new ExtendedChoiceParam(name: it.name, description: it.description, type: it.type, value: it.value, visibleItemCount: it.visibleItemCount, multiSelectDelimiter: it.delimiter))
                    }
                }
            }
            if(checkIfKeyExists(jobDetails, "bitbucketPullRequest")) {
                bitbucketPullRequestValues = new BitbucketPullRequest()
                bitbucketPullRequestValues.credID = project['gitCredentialId']
                bitbucketPullRequestValues.repoOwner = jobDetails.bitbucketPullRequest.repoOwner
                bitbucketPullRequestValues.repoName = jobDetails.bitbucketPullRequest.repoName
                bitbucketPullRequestValues.cronScheduling = jobDetails.bitbucketPullRequest.cron
            }

            new CiPipelineBuilder(
                    name: "$parent_folder/$project_name/$env/$it-pipeline",
                    buildsToKeep: jobDetails.buildsToKeep.equals(null)?10:jobDetails.buildsToKeep,
                    parameters: parameterList,
                    bitbucketPullRequestTrigger:bitbucketPullRequestValues,
                    label: envDetails.pipeline.jobs."$it".label,
                    bitbucketPush: jobDetails.bitbucketPush,
                    scmPollTrigger: jobDetails.scmPollTrigger,
                    cloneOptions: jobDetails.cloneOptions,
                    sparseCheckoutPath: jobDetails.sparseCheckoutPath,
                    gitLfsPull: jobDetails.gitLfsPull,
                    gitConfig: new GitConfig(gitRepository: project['gitRepository'], gitBranch: jobDetails.gitBranch, gitRefspec: jobDetails.gitRefspec, gitCredentialId: project['gitCredentialId']),
                    jenkinsFilePath: envDetails.pipeline.jobs."$it".jenkinsFilePath
            ).build(this)
        }

        if(project.needPipelineRunner) {s
            Class<Object> masterClass = (Class<Object>) Class.forName("pipelineRunner")
            masterClass.newInstance(
                    parent_folder: "$parent_folder",
                    project: project,
                    env: "$env"
            ).build(this)
        }
    }
}

def getConfigJson(def fileName) {
    hudson.FilePath workspace = hudson.model.Executor.currentExecutor().getCurrentWorkspace()
    File configFile = new File("${workspace}/$fileName")
    return new JsonSlurperClassic().parseText(configFile.text)
}

def getEnvironments(def directoryPath) {
    def list = []
    hudson.FilePath workspace = hudson.model.Executor.currentExecutor().getCurrentWorkspace()
    def dir = new File("${workspace}/$directoryPath")
    dir.eachFileRecurse(FileType.FILES) { file ->
        list << FilenameUtils.removeExtension(file.getName())
    }
    return list
}

def checkIfKeyExists(def json, def key){
    if( json.keySet().contains(key) ) {
        return true
    } else {
        return false
    }
}