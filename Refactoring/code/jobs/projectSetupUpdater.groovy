import groovy.json.JsonSlurperClassic

def allProjectDetails = new JsonSlurperClassic().parseText(readFileFromWorkspace('resources/com/itt/jenkins_job_dsl/repoConfig.json'))
def jobDslGitConfig = allProjectDetails['job-dsl']
def projects = allProjectDetails['projects'].keySet()

List<String> projectList = new ArrayList<String>(Arrays.asList("All"))
projectList.addAll(new ArrayList<String>(allProjectDetails['projects'].keySet()))

def parent_folder = "DevOps"
folder("${parent_folder}")
job("${parent_folder}/projectJobGenerator") {
    label 'master'
    parameters {
        choiceParam('ProjectToBeBuilt', projectList, 'Select a project to be built')
    }
    multiscm {
        git {
            remote {
                url(jobDslGitConfig['gitRepository'])
                credentials(jobDslGitConfig['gitCredentialId'])
            }
            branch(jobDslGitConfig['gitBranch'])
        }
        projects.each { project_name ->
            def project = allProjectDetails['projects'][project_name]
            git {
                remote {
                    url(project['gitRepository'])
                    credentials(project['gitCredentialId'])
                }
                branch(project['gitBranch'])
                extensions {
                    relativeTargetDirectory(project_name)
                }
            }
        }
    }
    steps {
        dsl {
            external "jobs/projectJobGenerator.groovy"
            additionalClasspath "src/"
        }
    }
}
