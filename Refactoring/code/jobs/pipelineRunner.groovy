import com.itt.jenkins_job_dsl.ExtendedChoiceParam
import groovy.json.JsonSlurperClassic
import javaposse.jobdsl.dsl.Job
import com.itt.jenkins_job_dsl.builder.CiJobBuilder
import com.itt.jenkins_job_dsl.config.GitConfig
import com.itt.jenkins_job_dsl.StringParam
import com.itt.jenkins_job_dsl.ChoiceParam
import com.itt.jenkins_job_dsl.PasswordParam
import com.itt.jenkins_job_dsl.publishers.ConditionalAction
import com.itt.jenkins_job_dsl.publishers.FlexiblePublish
import com.itt.jenkins_job_dsl.publishers.DownstreamParameterized
import com.itt.jenkins_job_dsl.steps.DownstreamParameterizedStep

class pipelineRunner {

    def parent_folder
    def project
    def env

    Job build(dslFactory) {
        dslFactory.with {
            def envDetails = getConfigJson(project['name'] + "/.jenkins/config/$env" + ".json")
            def jobNames = envDetails.pipeline.jobs.keySet()

            def parameters = []
            jobNames.each {
                parameters.clear()
                def jobDetails = envDetails.pipeline.jobs."$it"
                if (jobDetails.parameters) {
                    if (checkIfKeyExists(jobDetails.parameters, "string")) {
                        jobDetails.parameters.string.each {
                            parameters.add(new StringParam(name: it.key, defaultValue: it.value))
                        }
                    }
                    if (checkIfKeyExists(jobDetails.parameters, "choice")) {
                        jobDetails.parameters.choice.each {
                            parameters.add(new ChoiceParam(name: it.key, valueList: Arrays.asList(it.value.split("\\s*,\\s*"))))
                        }
                    }
                    if (checkIfKeyExists(jobDetails.parameters, "password")) {
                        jobDetails.parameters.password.each {
                            parameters.add(new PasswordParam(name: it.key, defaultValue: it.value))
                        }
                    }
                    if (checkIfKeyExists(jobDetails.parameters, "extendedChoice")) {
                        jobDetails.parameters.extendedChoice.each {
                            parameters.add(new ExtendedChoiceParam(name: it.name, description: it.descripton, type: it.type, value: it.value, visibleItemCount: it.visibleItemCount, multiSelectDelimiter: it.delimiter))
                        }
                    }
                }
            }

                def conditionalActions = []
                def publishers = []
                def buildSteps = []
                if (envDetails.pipelineRunner.conditionForTrigger) {
                    if (envDetails.pipelineRunner.conditionForTrigger.equals("modular")) {
                        jobNames.each { jobName ->
                            def completeJobName = "$parent_folder/" + project['name'] + "/$env/$jobName-pipeline"
                            def moduleName = envDetails.pipeline.jobs."$jobName".moduleName
                            conditionalActions.add(new ConditionalAction(
                                    expression: true,
                                    regEx: ".*$moduleName/.*",
                                    expLabel: '''${CHANGES, showPaths=true, format="%p", pathFormat="%p"}''',
                                    downstreamParameterized: true,
                                    downstream: new DownstreamParameterized(job: completeJobName)
                            )
                            )
                        }
                    } else if (envDetails.pipelineRunner.conditionForTrigger.equals("branch")) {
                        jobNames.each { jobName ->
                            def completeJobName = "$parent_folder/" + project['name'] + "/$env/$jobName-pipeline"
                            def regexForBranch = envDetails.pipeline.jobs."$jobName".regexForBranch
                            conditionalActions.add(new ConditionalAction(
                                    expression: true,
                                    regEx: "$regexForBranch",
                                    expLabel: '''$GIT_BRANCH''',
                                    downstreamParameterized: true,
                                    downstream: new DownstreamParameterized(job: completeJobName)
                            )
                            )
                        }
                    }
                    publishers.add(new FlexiblePublish(conditionalActions: conditionalActions))
                }else {
                        def jobList
                        if (!(envDetails.pipelineRunner.jobsToBeTriggered.equals("ALL"))) {
                            def jobsToBeTriggered = envDetails.pipelineRunner.jobsToBeTriggered.split(",")
                            jobList = jobsToBeTriggered.inject("") { string, jobName ->
                                string + (string ? ', ' : '') + "$parent_folder/" + project['name'] + "/$env/${jobName}-pipeline"
                            }
                        } else {
                            jobList = jobNames.inject("") { string, jobName ->
                                string + (string ? ', ' : '') + "$parent_folder/" + project['name'] + "/$env/${jobName}-pipeline"
                            }
                        }
                        buildSteps.add(new DownstreamParameterizedStep(job: jobList))
                    }
                    new CiJobBuilder(
                            name: "$parent_folder/" + project['name'] + "/$env/pipeline-runner",
                            label: "master",
                            parameters: parameters,
                            scms: [new GitConfig(gitRepository: project['gitRepository'], gitBranch: envDetails.pipelineRunner.gitBranch, gitRefspec: envDetails.pipelineRunner.gitRefspec, gitCredentialId: project['gitCredentialId'])],
                            bitbucketPush: envDetails.pipelineRunner.bitbucketPush,
                            scmPollTrigger: envDetails.pipelineRunner.scmPollTrigger,
                            buildSteps: buildSteps,
                            publishers: publishers
                    ).build(dslFactory)
            }
        }

    def getConfigJson(def fileName) {
        hudson.FilePath workspace = hudson.model.Executor.currentExecutor().getCurrentWorkspace()
        File configFile = new File("${workspace}/$fileName")
        return new JsonSlurperClassic().parseText(configFile.text)
    }

    def checkIfKeyExists(def json, def key){
        if( json.keySet().contains(key) ) {
            return true
        } else {
            return false
        }
    }
}