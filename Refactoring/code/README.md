# Jenkins pipeline framework #

Jenkins is a wonderful system for managing builds, and we love using its UI to configure jobs. As Jenkins is adopted for more and more projects in our organization, common patterns are likely to emerge. Unfortunately, as the number of jobs grows, maintaining them becomes tedious, and the paradigm of using a UI falls apart.
The Jenkins job-dsl-plugin attempts to solve this problem by allowing jobs to be defined with the absolute minimum necessary in a programmatic form, with the help of templates that are synced with the generated jobs.
With the introduction of the Pipeline plugin, now we can implement a project’s entire build/test/deploy pipeline in a Jenkinsfile and store that alongside their code, treating their pipeline as another piece of code checked into source control. Pipeline has support for creating "Shared Libraries" which can be defined in external source control repositories and loaded into existing Pipelines.

## Directory Structure ##
The directory structure of a job DSL repository, a shared library, should be as follows:

~~~~
(root)

+ - jobs      				        # Seed groovy files with job definition.

|     + - seed.groovy

+ - src					            # Groovy source files

|     + - org

|           + - foo

|           	+ - example.groovy	# Class files

+ - resources		              	# Resource files

|    	+ - repo_config.json       	# Configuration related to job DSL and project repositories

+ - vars			  	            # Scripts that define global variables accessible from Pipeline

|    	+ - example.txt

|    	+ - batch
~~~~

The jobs directory contains all the groovy files which holds the definition of the jobs to be created in Jenkins.
The src directory should look like standard Java source directory structure. This directory is added to the classpath when executing Pipelines.
The vars directory hosts scripts that define global variables accessible from Pipeline.
A resources directory allows the libraryResource step to be used from an external library to load associated non-Groovy files.

## Using Libraries ##
Shared Libraries marked Load implicitly in Jenkins configuration allows Pipelines to immediately use classes or global variables defined by any such libraries. To access other shared libraries, the Jenkinsfile needs to use the @Library annotation, specifying the library’s name:

@Library('somelib')

The annotation can be anywhere in the script where an annotation is permitted by Groovy. When referring to class libraries (with src/ directories), conventionally the annotation goes on an import statement:
@Library('somelib')
import com.mycorp.pipeline.somelib.UsefulClass

## Things to remember by DevOps Team ##
*	Above mentioned directory structure should be followed.
*	The job DSL repository should be added as Shared Library in Jenkins with ‘wire-pipeline’

## How to get a credential Id? ##
1. First add our jenkins credentials under global credentials.
2. Make sure, that our jenkins user can clone the private repo using this credential.
3. Now comes the important part, we dont see any mention of 'id' after we create our jenkins credentials, so the id is seen when we click on that specific credential we created in jenkins, see the url , the last field in the URL is the credential-id ..!!
yes , the 32-digit hexadecimal code is the credential id.
for example a credential-id looks like this : '7b274a40-4533-4d2d-a37f-917d5b785e97'


## How the .jenkins directory structure needs to be setup in the project repository? ##
Maintain the following directory structure in .jenkins directory along with the source code in the project repository

~~~~
(root)
+ - src                  #Standard project source code directory structure

+ - jenkins              # Separate Directory to keep configurations, scripts & Jenkinsfile

|   + - config           # Environment related configurations

|       + - dev.json

|       + - qa.json

|       + - prod.json

|   + - job-dsl          # Jenkinsfiles which defines the behavior of pipeline job

|       + - Jenkinsfile.build       

|       + - Jenkinsfile.deploy

|   + - scripts          # Respective scripts to be executed in Jenkinsfile(s)

|       + - build.sh

|       + - deploy.bat
~~~~

### Snapshot of configuration _json_ file and _Jenkinsfile_ ###
**Minimum Options** (referring above mentioned directory structure example)

![Minimum Config](docs/minimum_config_example.PNG)

**All Options** (referring above mentioned directory structure example)

![All Options Config](docs/all_options_config_example.PNG)

**Sample Jenkinsfile**

![Jenkinsfile](docs/jenkinsfile.PNG)