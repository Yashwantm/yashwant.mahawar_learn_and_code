package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Survay 
{
	
	static ArrayList<survayMap> survayList = new ArrayList<survayMap>();
	static ArrayList<sportsMap> sportsList = new ArrayList<sportsMap>();
	public static void main(String[] args) throws IOException 
	{
		Scanner sc = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int survayCount = sc.nextInt();
		for(int i = 0 ; i< survayCount ; i++)
		{
			System.out.println("Enter person name");
			String name = br.readLine();
			System.out.println("Enter person sports");
			String sportsName = br.readLine();
			boolean isSurvayTaken = checkSurvay(name,survayList);
			if(!isSurvayTaken)
			{
				survayMap survay = new survayMap();
				survay.setName(name);
				survay.setSportsName(sportsName);
				survayList.add(survay);
				updateSportsPopularity(sportsList,sportsName);
			}
		}
		sportsMap popularSport = findMaxPopularSports(survayList);
		printFootballAndMaxSports(sportsList,popularSport);
		sc.close();
	}
	private static void printFootballAndMaxSports(ArrayList<sportsMap> sportsList,sportsMap popularSport) 
	{
		int football = 0;
		for(int i=0;i<sportsList.size();i++)
		{
			if(sportsList.get(i).getSportsName().equalsIgnoreCase("football"))
			{
				football = sportsList.get(i).getSportsPopulirity();
				break;
			}
			
		}
			System.out.println("football "+football);
			System.out.println("popular sports "+popularSport.getSportsName());
	}
	private static void updateSportsPopularity(ArrayList<sportsMap> sportsList,String sportName)
	{
		int flag =0;
		for(int i =0;i<sportsList.size();i++)
		{
			if(sportsList.get(i).getSportsName().equals(sportName))
			{
				sportsList.get(i).setSportsPopulirity(sportsList.get(i).getSportsPopulirity()+1);
				flag =1;
				break;
			}
		}
		if(flag == 0)
		{
		sportsMap sports = new sportsMap();
		sports.setSportsName(sportName);
		sports.setSportsPopulirity(1);
		sportsList.add(sports);
		}
	}
	private static sportsMap findMaxPopularSports(ArrayList<survayMap> survayList) 
	{
		sportsMap maxPopularSports = new sportsMap();
		maxPopularSports.setSportsPopulirity(0);
		for(int i =0;i<sportsList.size();i++)
		{
			if(sportsList.get(i).getSportsPopulirity()>maxPopularSports.getSportsPopulirity())
			{
				maxPopularSports = sportsList.get(i);
			}
		}
		return maxPopularSports;
	}
	public static Boolean checkSurvay(String name,ArrayList<survayMap> survayList)
	{
		for(int i =0;i<survayList.size();i++)
		{
			if(survayList.get(i).getName().equals(name))
			{
				System.out.println("already taken");
				return true;
			}
		}
		return false;
	}
	
}
