package test;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
 
public class Statistics1
{
	public static BufferedReader bufferedReader;
	public static void main(String[] args) throws NumberFormatException, IOException 
	{
		bufferedReader= new BufferedReader(new InputStreamReader(System.in));
		int testCase;
		testCase = Integer.parseInt(bufferedReader.readLine());
		HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
		String sports="";
		
		getFriendNameAndFriendsSport(testCase, hashMap);
		sports = getMaxpopularSport(hashMap); 
		printMaxPopularSportAndFootballPopularity(hashMap, sports);
				
	}
	public static void getFriendNameAndFriendsSport(int testcase,HashMap<String, Integer> hashMap) throws IOException
	{
		while(testcase--!=0)
		{
			String nameAndSport[]= (bufferedReader.readLine()).split(" ");
			if(hashMap.containsKey(nameAndSport[1]))
			{
				Integer temp = hashMap.get(nameAndSport[1]);
				hashMap.replace(nameAndSport[1], temp+1);
			}
			else
			{
				if(validate(nameAndSport[1]))
				hashMap.put(nameAndSport[1], 1);
			}
		}
	}
	private static boolean validate(String name) {
		if(name.length()>0 && name.length()<12)
			return true;
		return false;
	}
	public static String getMaxpopularSport(HashMap<String, Integer> hashMap)
	{
		int m =0;
		String sports = "";
		for(String s : hashMap.keySet())
		{
			if(hashMap.get(s)>m)
				m=hashMap.get(s);
		}
		for(String s:hashMap.keySet())
		{
			if(hashMap.get(s)==m)
			{
				sports=s;
				break;
			}
		}
		return sports;
	}
	public static void printMaxPopularSportAndFootballPopularity(HashMap<String, Integer> hashMap,String sports)
	{
		if(hashMap.get("football")==null)
			System.out.println(sports+"\n"+0);
		else
		System.out.println(sports+"\n"+hashMap.get("football"));
	}
}