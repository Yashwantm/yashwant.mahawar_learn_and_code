package customCollectionFramework;
import java.util.ArrayList;
import java.util.LinkedList;

class HashNode<K,V>
{
	K key;
	V value;
	int hashCode;
	public HashNode(K key,V value) 
	{
		this.key = key;
		this.value = value;
	}
}
public class CustomHashMap<K,V> 
{
	K key;
	V value;
	final int BUCKET_SIZE = 10;
	ArrayList<LinkedList<HashNode<K,V>>> hashBucket = new ArrayList<LinkedList<HashNode<K,V>>>();
	public CustomHashMap() 
	{
		for(int index = 0; index < BUCKET_SIZE;index++)
		{
			hashBucket.add(null);
		}
	}
	public void put(K key, V value)
	{
		int currentHashCode = key.hashCode();
		if(currentHashCode<0)
			currentHashCode = currentHashCode * -1;
		int currentIndex = currentHashCode % 10;
		if(hashBucket.get(currentIndex)!=null)
		{
			LinkedList<HashNode<K,V>> currentLinkedList = hashBucket.get(currentIndex);
			currentLinkedList.add(new HashNode<K,V>(key,value));
		}
		else
		{
			LinkedList<HashNode<K,V>> temp = new LinkedList<HashNode<K,V>>();
			temp.add(new HashNode<K,V>(key, value));
			hashBucket.set(currentIndex,temp);
		}
	}
	public V get(K key)
	{
		int currentHashCode = key.hashCode();
		if(currentHashCode<0)
			currentHashCode = currentHashCode * -1;
		int currentIndex = currentHashCode % 10;
		if(hashBucket.get(currentIndex)!=null)
		{
			LinkedList<HashNode<K,V>> temp = hashBucket.get(currentIndex);
			for(HashNode<K,V> hashnode:temp)
			{
				if(hashnode.key.equals(key))
				{
					return hashnode.value;
				}
			}
		}
		return null;
	}
	public void printmap()
	{
		for(LinkedList<HashNode<K,V>> currentList:hashBucket)
		{
			if(currentList==null)
				continue;
			for(HashNode<K,V> currentNode:currentList)
			{
				System.out.print("<"+currentNode.key+","+currentNode.value+"> ");
			}
		}
	}
	public void remove(K key)
	{
		int currentHashCode = key.hashCode();
		if(currentHashCode<0)
			currentHashCode = currentHashCode * -1;
				int currentIndex = currentHashCode % 10;
				LinkedList<HashNode<K,V>> currentList = hashBucket.get(currentIndex);
				for(HashNode<K,V> currentHashNode:currentList)
				{
					if(currentHashNode.key.equals(key))
					{
						currentList.remove(currentHashNode);
					}
				}
	}
	public boolean containsKey(K key)
	{
		boolean containsResult=false;
		int currentHashCode = key.hashCode();
		if(currentHashCode<0)
			currentHashCode = currentHashCode * -1;
		int currentIndex = currentHashCode % 10;
		LinkedList<HashNode<K,V>> currentList = hashBucket.get(currentIndex);
		if(!(currentList == null))
		for(HashNode<K,V> currentHashNode:currentList)
		{
			if(currentHashNode.key.equals(key))
			{
				containsResult=true;
			}
		}
		return containsResult;
	}
	public void replace(K key,V value)
	{
		int currentHashCode = key.hashCode();
		if(currentHashCode<0)
			currentHashCode = currentHashCode * -1;
		int currentIndex = currentHashCode % 10;
		LinkedList<HashNode<K,V>> currentList = hashBucket.get(currentIndex);
		for(HashNode<K,V> currentHashNode:currentList)
		{
			if(currentHashNode.key.equals(key))
			{
				currentHashNode.value = value;
			}
		}
	}
	public ArrayList<K> keySet()
	{
		ArrayList<K> keyset = new ArrayList<>();
		for(LinkedList<HashNode<K,V>> currentList:hashBucket)
		{
			if(currentList==null)
				continue;
			for(HashNode<K,V> currentNode:currentList)
			{
				keyset.add(currentNode.key);
			}
		}	
		return keyset;
	}
}
