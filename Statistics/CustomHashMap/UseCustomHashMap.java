package customCollectionFramework;
public class UseCustomHashMap 
{
	
	public static void main(String[] args) {
		CustomHashMap<String, String> customHashMap = new CustomHashMap<String,String>();
		customHashMap.put("a1", "a1");
		customHashMap.put("a2", "a2");
		customHashMap.put("a3", "a3");
		customHashMap.put("b", "b1");
		customHashMap.put("b2", "b2");
		customHashMap.put("b3", "b3");
		System.out.println(customHashMap.get("b"));
		System.out.println(customHashMap.get("a2"));
		customHashMap.printmap();
		//customHashMap1.remove("b");
		customHashMap.printmap();
		
	}
}
