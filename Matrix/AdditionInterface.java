package matrix;

public interface AdditionInterface {
	 void insertIntoTwoDimensionlArray(String input) throws Exception;
	 String addAlternateArrayElement(int sampleInput[][]) throws Exception;
}
