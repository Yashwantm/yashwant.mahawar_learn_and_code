package matrix;

import static org.junit.Assert.*;

import org.junit.Test;

public class ArrayAdditionTest {

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void testValidateArrayInput_OutOfIndex() throws Exception
	{
		ArrayAddition arrayAddition = new ArrayAddition();
		String input = "1 2 3 4 5 6 7 8";
		
		arrayAddition.insertIntoTwoDimensionlArray(input);
		
	}
	@Test(expected = NumberFormatException.class)
	public void testValidateArrayInput_NumberFormat() throws Exception
	{
		ArrayAddition arrayAddition = new ArrayAddition();
		String input = "1 2 3 4 5 6 7 Y 9";
		
		arrayAddition.insertIntoTwoDimensionlArray(input);
		
	}
	public void testInsertIntoTwoDimensionlArray_CheckForNull() throws Exception{
		ArrayAddition arrayAddition = new ArrayAddition();
		String input = "1 2 3 4 5 6 7 8 9";
		
		arrayAddition.insertIntoTwoDimensionlArray(input);
		int sampleInput[][] = arrayAddition.getTwoDimensionalArrayData();
		
		assertNotNull(sampleInput[0][0]);
		assertNotNull(sampleInput[1][1]);
		assertNotNull(sampleInput[2][2]);
	}
	@Test
	public void testInsertIntoTwoDimensionlArray_CheckForValue()  throws Exception{
		ArrayAddition arrayAddition = new ArrayAddition();
		String input = "1 2 3 4 5 6 7 8 9";
		
		arrayAddition.insertIntoTwoDimensionlArray(input);
		int sampleInput[][] = arrayAddition.getTwoDimensionalArrayData();
		
		assertEquals(sampleInput[0][0], Integer.parseInt(input.split(" ")[0]));
		assertEquals(sampleInput[1][1], Integer.parseInt(input.split(" ")[4]));
		assertEquals(sampleInput[2][2], Integer.parseInt(input.split(" ")[8]));
	}
	@Test
	public void testAddAlternateArrayElement() throws Exception
	{
		ArrayAddition arrayAddition = new ArrayAddition();
		int sampleInput[][] = {{1,2,3},{4,5,6},{7,8,9}};
		
		String result = arrayAddition.addAlternateArrayElement(sampleInput);
		
		assertNotNull(result);
		assertEquals(result.split(" ")[0], "25");
		assertEquals(result.split(" ")[1], "20");
	}

}
